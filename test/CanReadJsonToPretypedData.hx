package test;
import hx.json.parser.UntypedJsonMap;
import haxe.ds.Either;
import hx.json.parser.ErrorDetail.ErrorDetailFollower;
import hx.streams.StringInput;
import buddy.BuddySuite;
using buddy.Should;

class CanReadJsonToPretypedData extends BuddySuite {

    public function new() {
        describe("Pretyping a Json", {
            it("should work with int numbers", {
                switch(new UntypedJsonMap(new StringInput("1"), true, false, true).parse()){
                    case Successful(IntValue(val)): val.should.be(1);
                    case Successful(data):
                        fail('${data} should be Int(1)');
                    case WithError(error):
                        ErrorDetailFollower.trace(error);
                        fail(error.message);
                }
                switch(new UntypedJsonMap(new StringInput("-17"), true, false, true).parse()){
                    case Successful(IntValue(val)): val.should.be(-17);
                    case Successful(data):
                        fail('${data} should be Int(-17)');
                    case WithError(error):
                        ErrorDetailFollower.trace(error);
                        fail(error.message);
                }
            });
            it("should work with float numbers", {
                switch(new UntypedJsonMap(new StringInput("-1.23e-15"), true, false, true).parse()){
                    case Successful(FloatValue(val)):
                        var precision = 17;
                        val.should.beCloseTo(-1.23e-15, precision);
                        val.should.not.beCloseTo(-1.24e-15, precision);
                    case Successful(data):
                        fail('${data} should be Float(-1.23e-15)');
                    case WithError(error):
                        ErrorDetailFollower.trace(error);
                        fail(error.message);
                }
            });
            it("should work with all types", {
                switch(new UntypedJsonMap(new StringInput('[1,null,{"h\\u0061llo":true}]'), true, false, true).parse()){
                    case Successful(ArrayValue([
                        IntValue(1),NullValue,ObjectValue(_["hallo"] => BooleanValue(true))
                    ])):
                    case Successful(data):
                        fail('${data} was not decoded correct!');
                    case WithError(error):
                        ErrorDetailFollower.trace(error);
                        fail(error.message);
                }
            });
            it("should work with non strict mode", {
                switch(new UntypedJsonMap(new StringInput('[,1 2null{,"hallo"true,},]'), false, false, true).parse()){
                    case Successful(ArrayValue([
                        IntValue(1),IntValue(2),NullValue,ObjectValue(_["hallo"] => BooleanValue(true))
                    ])):
                    case Successful(data):
                        fail('${data} was not decoded correct!');
                    case WithError(error):
                        ErrorDetailFollower.trace(error);
                        fail(error.message);
                }
            });
            it("should work return a error with wrong UTF-8 Escape-Sequence", {
                switch(new UntypedJsonMap(new StringInput('"\\u23X0"'), false, false, true).parse()){
                    case Successful(data):
                        fail('${data} an error was expected!');
                    case WithError(error):
                        //ErrorDetailFollower.trace(error);
                        error.col.should.be('"\\u23X'.length);
                        error.line.should.be(1);
                }
            });

            it("should work with additional text at the end", {
                var input = new StringInput("1bla");
                switch(new UntypedJsonMap(input, true, false, false).parse()){
                    case Successful(IntValue(val)):
                        val.should.be(1);
                        input.getNextChars(20).should.be("bla");
                    case Successful(data):
                        fail('${data} should be Int(1)');
                    case WithError(error):
                        ErrorDetailFollower.trace(error);
                        fail(error.message);
                }
            });
            it("should work with non strict mode and comments", {
                switch(new UntypedJsonMap(new StringInput('[,1/*3*/2/*/"bla"*/null{,"hallo"true,},]'), false, true, true).parse()){
                    case Successful(ArrayValue([
                        IntValue(1),IntValue(2),NullValue,ObjectValue(_["hallo"] => BooleanValue(true))
                    ])):
                    case Successful(data):
                        fail('${data} was not decoded correct!');
                    case WithError(error):
                        ErrorDetailFollower.trace(error);
                        fail(error.message);
                }
            });
            it("should work with recursive comments", {
                switch(new UntypedJsonMap(new StringInput('[(*3(*7*)4*),1/*3*/2/*(*"bla"*/*)6*/null{,"hallo"true,},]'), false, true, true).parse()){
                    case Successful(ArrayValue([
                        IntValue(1),IntValue(2),NullValue,ObjectValue(_["hallo"] => BooleanValue(true))
                    ])):
                    case Successful(data):
                        fail('${data} was not decoded correct!');
                    case WithError(error):
                        ErrorDetailFollower.trace(error);
                        fail(error.message);
                }
            });
        });
    }
}
