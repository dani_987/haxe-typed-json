package test;
using buddy.Should;

class MainTest implements buddy.Buddy<[
    test.CanReadANumber,
    test.StringInputWorksProperly,
    test.ErrorDetailWorksProperly,
    test.CanReadJsonToPretypedData,
    test.CanReadComplexJson,
]>{}
