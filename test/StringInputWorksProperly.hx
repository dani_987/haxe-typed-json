package test;
import hx.streams.StringInput;
import hx.streams.LocalizedInput;
import buddy.BuddySuite;
using buddy.Should;

class StringInputWorksProperly extends BuddySuite {

    public function new() {
        describe("Using a StringInput", {
            var input : LocalizedInput = new StringInput("ABCabc");
            it("should work previewing a String", {
                input.previewNextChars(3).should.be("ABC");
                input.isEof().should.be(false);
            });
            it("should work previewing a shorter String", {
                input.previewNextChars(2).should.be("AB");
                input.isEof().should.be(false);
            });
            it("should work previewing a longer String", {
                input.previewNextChars(6).should.be("ABCabc");
                input.isEof().should.be(false);
            });
            it("should work previewing a to long String", {
                input.previewNextChars(12).should.be("ABCabc");
                input.isEof().should.be(false);
            });
            it("should work reading a String", {
                input.getNextChars(3).should.be("ABC");
                input.isEof().should.be(false);
            });
            it("should work reading the last String", {
                input.getNextChars(3).should.be("abc");
                input.isEof().should.be(true);
            });
            it("then it should always return empty", {
                input.previewNextChars(3).should.be("");
                input.isEof().should.be(true);
                input.getNextChars(3).should.be("");
                input.isEof().should.be(true);
            });
        });
    }
}
