package test;
import hx.streams.StringInput;
import hx.json.parser.ErrorDetail.ErrorDetailFollower;
import buddy.BuddySuite;
using buddy.Should;

typedef ThreeFloats = {
    f1:Float,
    f2:Float,
    f3:Float,
};
class CanReadANumber extends BuddySuite {

    public function new() {
        describe("Reading a Number", {
            it("should work for input 1", {
                var read = new hx.json.TypedJson<Int>().parse(new StringInput('1'));
                switch(read){
                    case ResultData(data):
                        data.should.be(1);
                    case WithDataError(error): fail(error);
                    case WithParsingError(error):
                        ErrorDetailFollower.trace(error);
                        fail(error.message);
                }
            });
            it("should work from an array", {
                var read = new hx.json.TypedJson<Array<Int>>().parse(new StringInput('[0.9,2.1,3.04]'));
                switch(read){
                    case ResultData(data):
                        data.should.containExactly([1,2,3]);
                    case WithDataError(error): fail(error);
                    case WithParsingError(error):
                        ErrorDetailFollower.trace(error);
                        fail(error.message);
                }
            });
            it("should work from an list", {
                var read = new hx.json.TypedJson<List<Int>>().parse(new StringInput('[0.9,2.1,3.04]'));
                switch(read){
                    case ResultData(data):
                        data.pop().should.be(1);
                        data.pop().should.be(2);
                        data.pop().should.be(3);
                    case WithDataError(error): fail(error);
                    case WithParsingError(error):
                        ErrorDetailFollower.trace(error);
                        fail(error.message);
                }
            });
            it("should work from an object with a float", {
                var read = new hx.json.TypedJson<{someVal:Int}>().parse(new StringInput('{"someVal":.995}'));
                switch(read){
                    case ResultData(data):
                        data.someVal.should.be(1);
                    case WithDataError(error): fail(error);
                    case WithParsingError(error):
                        ErrorDetailFollower.trace(error);
                        fail(error.message);
                }
            });
            it("should work with small floats", {
                var read = new hx.json.TypedJson<ThreeFloats>().parse(new StringInput(
                    '{"f1":0.06418121606111526,"f2":0.0039678700268268585,"f3":0.8000000715255737}'
                ));
                switch(read){
                    case ResultData(data):
                        data.f1.should.beCloseTo(0.06418121606111526);
                        data.f2.should.beCloseTo(0.0039678700268268585);
                        data.f3.should.beCloseTo(0.8000000715255737);
                    case WithDataError(error): fail(error);
                    case WithParsingError(error):
                        ErrorDetailFollower.trace(error);
                        fail(error.message);
                }
            });
        });
    }
}
