package test;
import hx.json.parser.ErrorDetail;
import hx.streams.StringInput;
import hx.streams.LocalizedInput;
import buddy.BuddySuite;

using buddy.Should;
using StringTools;

class ErrorDetailWorksProperly extends BuddySuite {

    public function new() {
        describe("The ErrorDetailFollower", {
            var line1 = "aaAAAaaAAA";
            var line2 = "bbBBcbbBBB";

            var input_1 : LocalizedInput = new StringInput('$line1\n$line2');
            var read_1 = input_1.getNextChars(16);
            var input_2 : LocalizedInput = new StringInput('\n$line2');
            var read_2 = input_2.getNextChars(6);

            var target = "TestCase";
            var message = "SampleErrorMessage";

            it("should return an valid error", {
                var follower = new ErrorDetailFollower(target);
                follower.updateRead(read_1);
                var error : ErrorDetail = follower.createError(message, input_1);
                error.col.should.be(5);
                error.line.should.be(2);
                error.context[1].should.be( line1 );
                error.context[2].should.be( line2 );
                error.errorPointer.should.be("    ^");
                error.inTarget.should.be(target);
                error.message.should.be(message);

                //ErrorDetailFollower.trace(error);

            });
            it("should return an valid shortened error", {
                var follower = new ErrorDetailFollower(target, 2);
                follower.updateRead(read_1);
                var error : ErrorDetail = follower.createError(message, input_1);
                error.col.should.be(5);
                error.line.should.be(2);
                error.context[1].should.be(  "...AAAaa..."  );
                error.context[2].should.be(  "...BBcbb..."  );
                error.errorPointer.should.be("     ^");
                error.inTarget.should.be(target);
                error.message.should.be(message);

                //ErrorDetailFollower.trace(error);
            });
            it("should return an valid error with empty line", {
                var follower = new ErrorDetailFollower(target);
                follower.updateRead(read_2);
                var error : ErrorDetail = follower.createError(message, input_2);
                error.col.should.be(5);
                error.line.should.be(2);
                error.context[1].should.be(  ""  );
                error.context[2].should.be( line2 );
                error.errorPointer.should.be("    ^");
                error.inTarget.should.be(target);
                error.message.should.be(message);

                //ErrorDetailFollower.trace(error);
            });
            it("should return an valid shortened error with empty line", {
                var follower = new ErrorDetailFollower(target, 2);
                follower.updateRead(read_2);
                var error : ErrorDetail = follower.createError(message, input_2);
                error.col.should.be(5);
                error.line.should.be(2);
                error.context[1].should.be(  "..."  );
                error.context[2].should.be(  "...BBcbb..."  );
                error.errorPointer.should.be("     ^");
                error.inTarget.should.be(target);
                error.message.should.be(message);

                //ErrorDetailFollower.trace(error);
            });
        });
    }
}
