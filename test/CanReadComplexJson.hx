package test;
import hx.streams.StringInput;
import hx.json.parser.ErrorDetail.ErrorDetailFollower;
import buddy.BuddySuite;
using buddy.Should;

typedef SomeStruct = {hallo:String};
typedef SomeOtherStruct = { > SomeStruct, ?bla:Bool, hi:Array<Bool>, map: Map<String, Float>};

typedef FileGltfTextureInfo = {
    index: Int,
    ?texCoord: Int,
}

typedef FileGltfContent = {
    ?materials: Array<{
        ?pbrMetallicRoughness: {
            ?baseColorTexture: FileGltfTextureInfo,
            ?metallicRoughnessTexture: FileGltfTextureInfo,
        },
        ?emissiveTexture: FileGltfTextureInfo,
    }>,
};

class CanReadComplexJson extends BuddySuite {

    public function new() {
        describe("Reading complex Structures with mutliple TypeDefs", {
            it("should work", {
                var read = new hx.json.TypedJson<FileGltfContent>().parse(new StringInput(
                    '{"materials":[{"emissiveTexture":{"index":1}},{}]}'
                ));
                switch(read){
                    case ResultData(data):
                        data.materials.length.should.be(2);
                    case WithDataError(error): fail(error);
                    case WithParsingError(error):
                        hx.json.parser.ErrorDetail.ErrorDetailFollower.trace(error);
                        fail(error.message);
                }
            });
        });
        describe("Reading Anonymous Structures", {
            it("should work", {
                var read = new hx.json.TypedJson<{hallo:String}>().parse(new StringInput('{"hallo":"hi"}'));
                switch(read){
                    case ResultData(data):
                        data.hallo.should.be("hi");
                    case WithDataError(error): fail(error);
                    case WithParsingError(error):
                        hx.json.parser.ErrorDetail.ErrorDetailFollower.trace(error);
                        fail(error.message);
                }
            });
        });

        describe("Reading SomeStruct", {
            it("should work", {
                var read = new hx.json.TypedJson<SomeStruct>().parse(new StringInput('{"hallo":"hi"}'));
                switch(read){
                    case ResultData(data):
                        data.hallo.should.be("hi");
                    case WithDataError(error): fail(error);
                    case WithParsingError(error):
                        ErrorDetailFollower.trace(error);
                        fail(error.message);
                }
            });
        });

        describe("Reading SomeOtherStruct", {
            it("should work", {
                var read = new hx.json.TypedJson<SomeOtherStruct>().parse(
                    new StringInput('{"hallo":"hi","hi":[true,true,false,true],"map":{"1":15}}')
                );
                switch(read){
                    case ResultData(data):
                        data.hallo.should.be("hi");
                        data.bla.should.be(null);
                        data.hi.should.containExactly([true,true,false,true]);
                        data.map["1"].should.beCloseTo(15);
                        data.map["nonExistent"].should.be(null);
                    case WithDataError(error): fail(error);
                    case WithParsingError(error):
                        ErrorDetailFollower.trace(error);
                        fail(error.message);
                }
            });
        });
    }
}
