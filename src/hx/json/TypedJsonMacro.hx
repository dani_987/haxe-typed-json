package hx.json;

#if macro
import haxe.macro.Context;
import haxe.macro.Expr;
import haxe.macro.Type;
import hx.macro.MacroTypeHelper;
import hx.macro.MacroTypeHelper.IdentifierFromTypeNameBuilder;

using hx.macro.MacroTypeDecorderHelper;
using hx.macro.MacroDefineHelper;
using haxe.macro.Tools;

class TypedJsonMacro {
    static function build() : ComplexType {
		 switch (Context.getLocalType()) {
			case TInst(_.get() => {name: "TypedJson"}, params):
				return buildTypedJson("TypedJson", params);
			default:
				throw 'Illegal Type ${Context.getLocalType()}!';
		}
	}

    private static function buildTypedJson(nameStart: String, paramsType:Array<Type>) : ComplexType{
		var params = [for (t in paramsType) TPType(t.toComplexType())];
        var numParams = paramsType.length;
		var name = '${nameStart}__${paramsType.map((param) -> {param.toUniqueName();}).join('_I_')}';
		var pack = ["hx", "json"];

		if (!MacroTypeDecorderHelper.isDefinedAsTypeInPath(name, pack)) {
			Context.defineType({
				pack: pack,
				name: name,
				pos: MacroDefineHelper.getPosition(),
				params: [for (i in 0...numParams){name: 'T$i'}],
				kind: TDClass(),
				fields: getFields(params, paramsType),
			});
		}
        return TPath({pack: pack, name: name, params: params});
    }

	private static function decodeValue(val: Expr, path: Expr) : Expr {
		return macro {
			var res = $val;
			switch(res){
				case ResultData(data) : data;
				default: return cast res;
			}
		}
	}

	private static function createFunGetValue(
		nameBuilder: IdentifierFromTypeNameBuilder,
		newName: String,
		fields : Array<Field>,
		typeParam: TypeParam,
		type: DecodedType,
	) : Field {
		var decoder = macro return WithDataError($v{newName} + " not implemented yet!");
		switch(type.decoded){
			case TypeBool:
				decoder = macro {
					switch(data){
						case BooleanValue(val): return ResultData(val);
						case _: return WithDataError('"$path" should be BooleanValue, but was ${data.getName()}');
					}
				}
			case TypeInt:
				decoder = macro {
					switch(data){
						case IntValue(val): return ResultData(val);
						case FloatValue(val): return ResultData(Math.round(val));
						case _: return WithDataError('"$path" should be IntValue or FloatValue, but was ${data.getName()}');
					}
				}
			case TypeFloat:
				decoder = macro {
					switch(data){
						case IntValue(val): return ResultData(val * 1.0);
						case FloatValue(val): return ResultData(val);
						case _: return WithDataError('"$path" should be IntValue or FloatValue, but was ${data.getName()}');
					}
				}
			case TypeString:
				decoder = macro {
					switch(data){
						case StringValue(val):
							return ResultData(val);
						case _: return WithDataError('"$path" should be StringValue, but was ${data.getName()}');
					}
				}
			case TypeMap(ofKey, ofValue):
				var typeParams = [
					TPType(ofKey.realType.toComplexType()),
					TPType(ofValue.realType.toComplexType())
				];
				var set : Expr = if(ofValue.nullable){ macro if(data != null) result[name] = data; }
					else { macro result[name] = data; }
				decoder = macro {
					var result = ${
						ENew({
							pack: [],
							name: "Map",
							params: typeParams,
						}, []).toExpr()
					}
					switch(data){
						case ObjectValue(map):
							for(name => parsed in map){
								var val = $i{getFunValueAsType(
										nameBuilder,
										fields,
										typeParams[1],
										ofValue
									)}(path + "[" + name + "]", parsed);
								switch(val){
									case ResultData(data):
										$set;
									case _:
								}
							}
						default: return WithDataError('"$path" should be ObjectValue, but was ${data.getName()}');

					}
					return ResultData(result);
				}
			case TypeArray(of):
				var typeParams = [TPType(of.realType.toComplexType())];
				var push : Expr = if(of.nullable){ macro if(data != null) result.push(data); }
					else { macro result.push(data); }
				decoder = macro {
					var result = ${
						ENew({
							pack: [],
							name: "Array",
							params: typeParams,
						}, []).toExpr()
					}
					switch(data){
						case ArrayValue(list):
							for(index => parsed in list){
								var val = $i{getFunValueAsType(
										nameBuilder,
										fields,
										typeParams[0],
										of
									)}(path + "[" + index + "]", parsed);
								switch(val){
									case ResultData(data):
										$push;
									case _: return cast val;
								}
							}
						default: return WithDataError('"$path" should be ObjectValue, but was ${data.getName()}');

					}
					return ResultData(result);
				}
			case TypeList(of):
				var typeParams = [TPType(of.realType.toComplexType())];
				var add : Expr = if(of.nullable){ macro if(data != null) result.add(data); }
					else { macro result.add(data); }
				decoder = macro {
					var result = ${
						ENew({
							pack: [],
							name: "List",
							params: typeParams,
						}, []).toExpr()
					}
					switch(data){
						case ArrayValue(list):
							for(index => parsed in list){
								var val = $i{getFunValueAsType(
										nameBuilder,
										fields,
										typeParams[0],
										of
									)}(path + "[" + index + "]", parsed);
								switch(val){
									case ResultData(data):
										$add;
									case _: return cast val;
								}
							}
						default: return WithDataError('"$path" should be ObjectValue, but was ${data.getName()}');

					}
					return ResultData(result);
				}
			case TypeStruct(structFields):
				var vars: Array<Var> = [];
				var objFields: Array<ObjectField> = [];
				for(field in structFields){
					var name = "_" + field.name;
					vars.push({
						name:name,
						namePos: MacroDefineHelper.getPosition(),
						type: field.realType.toComplexType(),
						expr: decodeValue(macro {
							$i{
								getFunValueAsType(
									nameBuilder,
									fields,
									TPType(field.realType.toComplexType()),
									field,
								)
							}(
								path + "." + $v{field.name},
								if(map.exists($v{field.name}))map[$v{field.name}] else null,
							);
						}, macro path + "." + $v{field.name}),
					});
					objFields.push({
						field: field.name,
						expr: macro $i{name},
					});
				}
				var construct : Expr = EObjectDecl(objFields).toExpr();
				decoder = macro {
					switch(data){
						case ObjectValue(map):
							${EVars(vars).toExpr()}
							return ResultData($construct);
						default: return WithDataError('"$path" should be ObjectValue, but was ${data.getName()}');
					}
				}
			default:
				trace(type.decoded);
				throw '${type.decoded.getName()} is not supported currently';
		}
		var onNull : Expr =
			if(type.nullable) macro return ResultData(null);
			else macro {
				return WithDataError('$path should not be null but is null.');
			}
		return newName.toPrivateFunction(
			[
				{name: 'path', type: macro : String},
				{name: 'data', type: macro : hx.json.parser.ParserResult.ParsedDataType},
			], macro {
				if(data == null || data.match(NullValue)){
					$onNull;
				}
				$decoder;
			},
			TPath({
				pack: ["hx", "json", "parser"],
				name: "ParserResult",
				params: [typeParam],
			})
		);
	}

	private static function getFunValueAsType(nameBuilder: IdentifierFromTypeNameBuilder, fields : Array<Field>, typeParam: TypeParam, type:DecodedType) : String {
		var newNameObj = nameBuilder.create(type);
		var newName = newNameObj.get();
		if(nameBuilder.isNew(newNameObj)){
			fields.push(createFunGetValue(nameBuilder, newName, fields, typeParam, type));
		}
		return newName;
	}

	private static function getFields(params:Array<TypeParam>, paramsType:Array<Type>) : Array<Field> {
		var nameBuilder = MacroTypeHelper.createNameFromTypeBuilder("getValue");
		var fields : Array<Field> = [];
		fields.push("new".toPublicFunction([], macro {}, macro : Void));
		fields.push("parse".toPublicFunction([
			{name: 'input', type: macro : hx.streams.LocalizedInput}
		], macro {
			switch(new hx.json.parser.UntypedJsonMap( input, true, false, true ).parse()){
				case WithError(error): return WithParsingError(error);
				case Successful(data):
					return $i{getFunValueAsType(nameBuilder, fields, params[0], paramsType[0].decode())}("$", data);
			}
		}, TPath({
			pack: ["hx", "json", "parser"],
			name: "ParserResult",
			params: params,
		})));

		return fields;
	}
}

#end
