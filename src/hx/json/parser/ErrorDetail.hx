package hx.json.parser;

import hx.streams.LocalizedInput;

using StringTools;

typedef ErrorDetail = {
    > Position,
    message: String,
    context: Map<Int, String>,
    errorPointer: String,
};

typedef Position = {
    line: Int,
    col: Int,
    inTarget: String,
};


enum CheckStatus {
    NewLineFound;
    CarriageReturnFound;
    NormalModus;
}

class ErrorDetailFollower {
    private var position: Position;
    private var thisLine: StringBuf;
    private var lastLine: String;

    private var contextLength:Int;

    public function new(inTarget: String, contextLength:Int = 15) {
        this.position = {
            inTarget: inTarget,
            line: 1,
            col: 0,
        };
        this.thisLine = new StringBuf();
        this.lastLine = "";
        this.contextLength = contextLength;
    }

    private function newLine(){
        this.position.line++;
        this.position.col = 0;
        this.lastLine = this.thisLine.toString();
        this.thisLine = new StringBuf();
    }

    public function updateRead(read: String) {
        var toCheck = read.substr(0);
        var modus : CheckStatus = NormalModus;
        while(toCheck.length > 0){
            var toCheckChar = toCheck.charAt(0);
            switch(toCheckChar){
                case "\n":
                    switch(modus){
                        case CarriageReturnFound: modus = NormalModus;
                        default: modus = NewLineFound; newLine();
                    }
                case "\r":
                    switch(modus){
                        case NewLineFound: modus = NormalModus;
                        default: modus = CarriageReturnFound; newLine();
                    }
                default:
                    modus = NormalModus;
                    this.position.col++;
                    this.thisLine.add(toCheckChar);
            }
            toCheck = toCheck.substr(1);
        }
    }

    public function createError(message: String, input: LocalizedInput) : ErrorDetail {
        var toCheck = input.previewNextChars(contextLength);
        var actualLine = this.thisLine.toString();
        var lineBefore = this.lastLine;
        var errorPointer: String = "^".lpad(" ", this.position.col);
        if(lineBefore.length > contextLength + this.position.col){
            lineBefore = lineBefore.substr(0, contextLength + this.position.col) + "...";
        }
        var cutBefore = actualLine.length - (contextLength + 1);
        if(cutBefore > 0){
            actualLine =   "..." + actualLine.substr(cutBefore);
            lineBefore =   "..." + lineBefore.substr(cutBefore);
            errorPointer = "   " + errorPointer.substr(cutBefore);
        }
        var readLineEnd = toCheck.length < contextLength;
        while(toCheck.length > 0){
            var toCheckChar = toCheck.charAt(0);
            switch(toCheckChar){
                case "\n" | "\r":
                    toCheck = "";
                    readLineEnd = true;
                default:
                    actualLine += toCheckChar;
            }
            toCheck = toCheck.substr(1);
        }
        if(!readLineEnd){
            actualLine += "...";
        }
        var context : Map<Int, String>;
        if(this.position.line > 1){
            context = [
                this.position.line - 1 => lineBefore,
                this.position.line     => actualLine,
            ];
        } else {
            context = [
                this.position.line     => actualLine,
            ];
        }
        return {
            inTarget: this.position.inTarget,
            line: this.position.line,
            col: this.position.col,
            context: context,
            message: message,
            errorPointer: errorPointer,
        };
    }

    public static function trace(detail: ErrorDetail){
        #if sys
        for(line in traceToArray(detail))Sys.println(line);
        #else
        for(line in traceToArray(detail))trace(line);
        #end
    }

    public static function traceToArray(detail: ErrorDetail) : Array<String>{
        var result = [
            "".lpad("#", 15),
            'ERROR in ${detail.inTarget} @${detail.line},${detail.col}: ${detail.message}',
        ];
        for(line => content in detail.context) result.push('$line'.lpad(" ", 5) + ' | $content');
        result.push(''.lpad("-", 5) + ' + ${detail.errorPointer}');
        result.push("".lpad("#", 15));
        return result;
    }
}
