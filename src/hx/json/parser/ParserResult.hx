package hx.json.parser;

enum UntypedParsingResult {
    Successful(data: ParsedDataType);
    WithError(error: ErrorDetail);
}

enum ParserResult<T> {
    ResultData(data: T);
    WithParsingError(error: ErrorDetail);
    WithDataError(error: String);
}

enum ParsedDataType {
    BooleanValue(val: Bool);
    StringValue(val: String);
    IntValue(val: Int);
    FloatValue(val: Float);
    ArrayValue(val: Array<ParsedDataType>);
    ObjectValue(val: Map<String, ParsedDataType>);
    NullValue;
}
