package hx.json.parser;

import hx.json.parser.ParserResult.UntypedParsingResult;
import hx.json.parser.ParserResult.ParsedDataType;
import hx.json.parser.ErrorDetail.ErrorDetailFollower;
import hx.streams.LocalizedInput;

class UntypedJsonMap {
    private var input: LocalizedInput;
    private var forceEof: Bool;
    private var follower: ErrorDetailFollower;
    private var strict: Bool;
    private var allowComments: Bool;

    public function new(
        input: LocalizedInput,
        strict: Bool,
        allowComments: Bool,
        forceEof: Bool,
    ) {
        this.input = input;
        this.follower = new ErrorDetailFollower(input.getSourceName());
        this.forceEof = forceEof;
        this.strict = strict;
        this.allowComments = allowComments;
    }

    public function parse() : UntypedParsingResult {
        var result = parseValue();
        if(result.match(Successful(_)) && !input.isEof() && forceEof){
            skipWhitespaces();
            follower.updateRead(input.getNextChars(1));
            if(!input.isEof()){
                return WithError(
                    this.follower.createError("Expected End-Of-Input!", this.input)
                );
            }
        }
        return result;
    }

    private function skipComment() {
        follower.updateRead(input.getNextChars(2));
        while(input.previewNextChars(2) != "*/" && !input.isEof()){
            if(input.previewNextChars(2) == "(*"){
                skipRecursiveComment();
            }
            follower.updateRead(input.getNextChars(1));
        }
        follower.updateRead(input.getNextChars(1));
    }

    private function skipRecursiveComment() {
        follower.updateRead(input.getNextChars(2));
        while(input.previewNextChars(2) != "*)" && !input.isEof()){
            if(input.previewNextChars(2) == "(*"){
                skipRecursiveComment();
            }
            follower.updateRead(input.getNextChars(1));
        }
        follower.updateRead(input.getNextChars(1));
    }

    private function skipWhitespaces() {
        while(!input.isEof()){
            if(! StringTools.isSpace(input.previewNextChars(1), 0)){
                if(allowComments && input.previewNextChars(2) == "/*"){
                    skipComment();
                } else if(allowComments && input.previewNextChars(2) == "(*"){
                    skipRecursiveComment();
                } else {
                    break;
                }
            }
            follower.updateRead(input.getNextChars(1));
        }
    }

    private function parseValue() : UntypedParsingResult {
        skipWhitespaces();
        if(input.isEof()){
            return WithError(
                this.follower.createError("Unexpected End-Of-Input, expected some Value!", this.input)
            );
        }
        if(input.previewNextChars(1) == '"' ){
            return parseString();
        } else if(input.previewNextChars(1) == '[' ){
            return parseArray();
        } else if(input.previewNextChars(1) == '{' ){
            return parseObject();
        } else if(isBetween("0", input.previewNextChars(1), "9") >= 0  || input.previewNextChars(1) == '-' || input.previewNextChars(1) == '.'){
            return parseNumber();
        } else if(!strict && input.previewNextChars(1) == '+'){
            return parseNumber();
        } else if(input.previewNextChars(4) == 'true'){
            follower.updateRead(input.getNextChars(4));
            return Successful(BooleanValue(true));
        } else if(input.previewNextChars(4) == 'null'){
            follower.updateRead(input.getNextChars(4));
            return Successful(NullValue);
        } else if(input.previewNextChars(5) == 'false'){
            follower.updateRead(input.getNextChars(5));
            return Successful(BooleanValue(false));
        } else {
            var next = input.getNextChars(1);
            follower.updateRead(next);
            return WithError(
                this.follower.createError('Expected some Value ([-0-9], true, false, {, [, \"), got $next!', this.input)
            );
        }
    }

    private function parseArray() : UntypedParsingResult {
        var next = input.getNextChars(1);
        follower.updateRead(next);
        var resultArray = [];
        if(next != '['){
            return WithError(
                this.follower.createError('Expected an Array ([), found "$next"!', this.input)
            );
        }
        while(!input.isEof()){
            skipWhitespaces();
            switch(input.previewNextChars(1)){
                case ",":
                    follower.updateRead(input.getNextChars(1));
                    if(strict){
                        return WithError(
                            this.follower.createError("Unexpected ',', expected some Value or ]!", this.input)
                        );
                    }
                case "]":
                    follower.updateRead(input.getNextChars(1));
                    return Successful(ArrayValue(resultArray));
                default:
                    var readValue = parseValue();
                    switch (readValue) {
                        case WithError(_): return readValue;
                        case Successful(data): resultArray.push(data);
                    }
                    skipWhitespaces();
                    if(input.previewNextChars(1) == ","){
                        follower.updateRead(input.getNextChars(1));
                    } else if(input.previewNextChars(1) == "]"){
                        follower.updateRead(input.getNextChars(1));
                        return Successful(ArrayValue(resultArray));
                    } else if (strict){
                        next = input.getNextChars(1);
                        follower.updateRead(next);
                        return WithError(
                            this.follower.createError('Expected ",", found "$next"!', this.input)
                        );
                    }
            }
        }
        return WithError(
            this.follower.createError("Unexpected End-Of-Input during Array parsing!", this.input)
        );

    }

    private function parseObject() : UntypedParsingResult {
        var next = input.getNextChars(1);
        follower.updateRead(next);
        var resultMap : Map<String, ParsedDataType> = [];
        if(next != '{'){
            return WithError(
                this.follower.createError('Expected an Object ({), found "$next"!', this.input)
            );
        }
        while(!input.isEof()){
            skipWhitespaces();
            switch(input.previewNextChars(1)){
                case ",":
                    follower.updateRead(input.getNextChars(1));
                    if(strict){
                        return WithError(
                            this.follower.createError("Unexpected ',', expected some Value or ]!", this.input)
                        );
                    }
                case "}":
                    follower.updateRead(input.getNextChars(1));
                    return Successful(ObjectValue(resultMap));
                case '"':
                    var readName = parseString();
                    switch (readName) {
                        case WithError(_): return readName;
                        case Successful(StringValue(name)):
                            skipWhitespaces();
                            next = input.previewNextChars(1);
                            if(next != ":" && strict){
                                follower.updateRead(input.getNextChars(1));
                                return WithError(
                                    this.follower.createError('Expected ":", found "$next"!', this.input)
                                );
                            } else if(next == ":") {
                                follower.updateRead(input.getNextChars(1));
                            }
                            skipWhitespaces();
                            var readValue = parseValue();
                            switch (readValue) {
                                case WithError(_): return readValue;
                                case Successful(data): resultMap[name] = data;
                            }
                        case Successful(type): return WithError(
                            this.follower.createError('Expected Datatype String, not ${type}!', this.input)
                        );
                    }
                    skipWhitespaces();
                    if(input.previewNextChars(1) == ","){
                        follower.updateRead(input.getNextChars(1));
                    } else if(input.previewNextChars(1) == "}"){
                        follower.updateRead(input.getNextChars(1));
                        return Successful(ObjectValue(resultMap));
                    } else if (strict){
                        next = input.getNextChars(1);
                        follower.updateRead(next);
                        return WithError(
                            this.follower.createError('Expected ",", found "$next"!', this.input)
                        );
                    }
                default:
                    next = input.getNextChars(1);
                    follower.updateRead(next);
                    return WithError(
                        this.follower.createError('Expected "}" or ", found "$next"!', this.input)
                    );
            }
        }
        return WithError(
            this.follower.createError("Unexpected End-Of-Input during Object parsing!", this.input)
        );
    }

    private function parseNumber() : UntypedParsingResult {
        var next : String;
        var number : Int = 0;
        var isFloat = false;
        var afterKomma : Float = 0;
        var countAfterKomma : Float = 1.0;
        var afterExponent : Int = 0;
        var withExponent = false;
        var isExponentNegative = false;
        var isNegative = input.previewNextChars(1) == '-';
        if(isNegative || ( !strict && input.previewNextChars(1) == '+' ) ){
            follower.updateRead(input.getNextChars(1));
        }
        switch(isBetween("0", input.previewNextChars(1), '9')){
            case 0:
                follower.updateRead(input.getNextChars(1));
            case _ => digit if (digit > 0):
                number = digit;
                follower.updateRead(input.getNextChars(1));
                while(!input.isEof()){
                    switch(isBetween("0", input.previewNextChars(1), '9')){
                        case _ => otherDigits if (otherDigits >= 0):
                            number = (number * 10) + otherDigits;
                            follower.updateRead(input.getNextChars(1));
                        default: break;
                    }
                }
            default:
        }

        if(input.previewNextChars(1) == "."){
            isFloat = true;
            follower.updateRead(input.getNextChars(1));
            while(!input.isEof()){
                next = input.previewNextChars(1);
                switch(isBetween("0", input.previewNextChars(1), '9')){
                    case _ => digit if (digit >= 0):
                        follower.updateRead(input.getNextChars(1));
                        countAfterKomma/=10;
                        afterKomma += digit * countAfterKomma;
                    default: break;
                }
            }
            if(countAfterKomma <= 0){
                next = input.getNextChars(1);
                follower.updateRead(next);
                return WithError(
                    this.follower.createError('Expected a Digit after ".", found "\\u$next"!', this.input)
                );
            }
        }

        if(input.previewNextChars(1) == "e" || input.previewNextChars(1) == "E"){
            isFloat = true;
            follower.updateRead(input.getNextChars(1));
            if(input.previewNextChars(1) == "+" || input.previewNextChars(1) == "-"){
                next = input.getNextChars(1);
                follower.updateRead(next);
                isExponentNegative = next == '-';
            }
            while(!input.isEof()){
                next = input.previewNextChars(1);
                switch(isBetween("0", input.previewNextChars(1), '9')){
                    case _ => digit if (digit >= 0):
                        follower.updateRead(input.getNextChars(1));
                        afterExponent = (afterExponent * 10) + digit;
                        withExponent = true;
                    default: break;
                }
                if(!withExponent){
                    next = input.getNextChars(1);
                    follower.updateRead(next);
                    return WithError(
                        this.follower.createError('Expected a Digit in Exponent, found "\\u$next"!', this.input)
                    );
                }
            }
        }
        if(!isFloat){
            if(isNegative){
                number *= -1;
            }
            return Successful(IntValue(number));
        }
        var resultFloat : Float = number;
        resultFloat += afterKomma;
        if(withExponent){
            if(isExponentNegative)afterExponent *= -1;
            resultFloat *= Math.pow(10.0, afterExponent);
        }

        if(isNegative){
            resultFloat *= -1;
        }
        return Successful(FloatValue(resultFloat));
    }

    private function parseString() : UntypedParsingResult {
        var next = input.getNextChars(1);
        follower.updateRead(next);
        var resultString = new StringBuf();
        if(next != '"'){
            return WithError(
                this.follower.createError('Expected a String ("), found "$next"!', this.input)
            );
        }
        while(!input.isEof()){
            next = input.getNextChars(1);
            follower.updateRead(next);
            switch(next){
                case '"': break;
                case "\\":
                    next = input.getNextChars(1);
                    follower.updateRead(next);
                    switch(next){
                        case "\\" | "/" | '"' : resultString.add(next);
                        case "b" : resultString.add("\x08");
                        case "f" : resultString.add("\x0C");
                        case "n" : resultString.add("\n");
                        case "r" : resultString.add("\r");
                        case "t" : resultString.add("\t");
                        case "u" :
                            next = input.previewNextChars(4);
                            if(! appendFromHex(resultString, 4) ){
                                return WithError(
                                    this.follower.createError('Expected a Unicode-Sign (\\uXXXX, X=[0-9a-fA-F]), found "\\u$next"!', this.input)
                                );
                            }
                    }
                default: resultString.add(next);
            }
        }

        return Successful(StringValue(resultString.toString()));
    }

    private function isBetween(from: String, between: String, to: String) : Int {
        if(from <= between && between <= to){
            return between.charCodeAt(0) - from.charCodeAt(0);
        }
        return -1;
    }

    private function appendFromHex(toAppendTo: StringBuf, length: Int) : Bool {
        if(input.previewNextChars(length).length != length)return false;
        var newCharCode = 0;
        var next = "";
        for(_ in 0...length){
            next = input.getNextChars(1);
            follower.updateRead(next);
            switch(next){
                case isBetween("0", _, "9") => num if (num >= 0) :
                    newCharCode = (newCharCode << 4) + num;
                case isBetween("a", _, "f") => hex if (hex >= 0) :
                    newCharCode = (newCharCode << 4) + hex + 10;
                case isBetween("A", _, "F") => hex if (hex >= 0) :
                    newCharCode = (newCharCode << 4) + hex + 10;
                default: return false;
            }
        }
        toAppendTo.addChar(newCharCode);
        return true;
    }
}
