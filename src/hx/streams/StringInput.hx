package hx.streams;

class StringInput extends LocalizedHaxeInput {
    public function new(ofString: String, sourceName: String = "<Unknown String-Source>") {
        super(new haxe.io.StringInput(ofString), sourceName);
    }
}
