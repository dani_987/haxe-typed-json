package hx.streams;

abstract class LocalizedHaxeInput implements LocalizedInput {
    private var input:haxe.io.Input;
    private var dataBuffer: String;
    private var sourceName: String;
    private var readEof: Bool;
    private var requestedLastBytes: Bool;

    private function new(input:haxe.io.Input, sourceName: String) {
        dataBuffer = "";
        readEof = false;
        requestedLastBytes = readEof;
        this.input = input;
        this.sourceName = sourceName;
    }


    public function getSourceName():String { return sourceName; }


    public function getNextChars(length:Int):String {
        var result = previewNextChars(length);
        if(length >= dataBuffer.length){
            requestedLastBytes = readEof;
            dataBuffer = "";
        } else dataBuffer = dataBuffer.substr(result.length);
        return result;
    }

    public function previewNextChars(length:Int):String {
        if(length > dataBuffer.length){
            dataBuffer += readString(length - dataBuffer.length);
        }
        return dataBuffer.substr(0, length);

    }

    private function readString(length:Int):String {
        var bytesBuffer = haxe.io.Bytes.alloc(length);
        var bytesToRead = length;
        var bytesRead = 0;
        try{
            while (bytesToRead > 0 && !readEof) {
                var bytesNowRead = input.readBytes(bytesBuffer, bytesRead, bytesToRead);
                if (bytesNowRead == 0)
                    readEof = true;
                bytesRead += bytesNowRead;
                bytesToRead -= bytesNowRead;
            }
        }catch (_: haxe.io.Eof) {readEof = true;}
        #if neko
		return neko.Lib.stringReference(bytesBuffer);
		#else
		return bytesBuffer.getString(0, bytesRead);
		#end
    }

    public function isEof():Bool {
        return requestedLastBytes;
    }
}
