package hx.streams;

interface LocalizedInput {
    public function getSourceName() : String;
    public function getNextChars(length: Int) : String;
    public function previewNextChars(length: Int) : String;
    public function isEof() : Bool;
}
