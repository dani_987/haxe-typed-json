package hx.streams;

#if sys

import sys.io.File;

class FileInput extends LocalizedHaxeInput {
    public function new(fileName: String) {
        super(File.read(fileName), '"$fileName"');
    }
}

#end
